import { NgModule, AfterContentInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpModule } from '@angular/http';

import { AppRoutingModule, routedComponents } from './app.routing';

import { AppComponent } from './app.component';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CommonModule,
    HttpModule,

    AppRoutingModule,
  ],
  declarations: [],
  providers: [/* TODO: Providers go here */],
  bootstrap: routedComponents,
})
export class AppModule implements AfterContentInit {

  constructor() { }

  ngAfterContentInit() { }
}
