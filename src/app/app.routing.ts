import { NgModule } from '@angular/core';
import { APP_BASE_HREF, CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';

import {
  MatCardModule,
  MatButtonModule,
  MatToolbarModule,
  MatInputModule,
  MatIconModule,
  MatTableModule,
  MatProgressBarModule,
  MatPaginatorModule,
} from '@angular/material';

import { DemoRoutingModule } from './theme/demo/demo.routing';

/** my components */
import { AppComponent } from './app.component';
import { ImpressumComponent } from './pages/impressum/impressum.component';
import { SearchPageComponent } from './pages/search/search-page.component';

/** my services */
import { RestClient } from './services/client/client.service';

const routes: Routes = [
  { path: '', component: SearchPageComponent },
  { path: 'impressum', component: ImpressumComponent },
  { path: '**', redirectTo: '' },
];

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatCardModule,
    MatInputModule,
    MatIconModule,
    MatProgressBarModule,
    MatToolbarModule,
    MatTableModule,
    MatPaginatorModule,
    RouterModule.forRoot(routes),
    DemoRoutingModule,
  ],
  declarations: [AppComponent, ImpressumComponent, SearchPageComponent],
  providers: [RestClient, { provide: APP_BASE_HREF, useValue: '/' }],
  exports: [RouterModule],
})
export class AppRoutingModule { }

export const routedComponents = [AppComponent];
