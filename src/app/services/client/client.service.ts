import { Injectable } from '@angular/core';
import {
  Http,
  Headers,
  Request,
  RequestMethod,
  RequestOptions,
  RequestOptionsArgs,
  URLSearchParams,
} from '@angular/http';

import * as rx from 'rxjs';
import * as model from '../model';

@Injectable()
export class RestClient {

  private baseURL = 'http://192.168.179.113:8080';

  constructor(private http: Http) { }

  makeHttpRequest(method: RequestMethod, url: string, options?: RequestOptionsArgs): rx.Observable<any> {
    const requestOptions = new RequestOptions(Object.assign({
      'method': method,
      'url': url,
    }, options));

    if (!requestOptions.headers) {
      requestOptions.headers = new Headers();
    }
    return this.http.request(new Request(requestOptions)).map(res => res.json());
  }

  getSearchResults(query?: string): rx.Observable<model.SearchResult[]> {
    const searchParams = new URLSearchParams();

    if (query) {
      searchParams.append('query', query);
    }
    return this.makeHttpRequest(
      RequestMethod.Get,
      `${this.baseURL}/repositories/search`,
      {
        search: searchParams,
      },
    );
  }
}
