export interface SearchResult {
  name?: string;
  url?: string;
  type?: string;
  trend?: number;
}
