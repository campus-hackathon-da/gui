import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DemoComponent } from './demo.component';

const routes: Routes = [
  { path: 'demo', component: DemoComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  declarations: [DemoComponent],
  exports: [RouterModule],
})
export class DemoRoutingModule { }

export const routedComponents = [DemoComponent];
