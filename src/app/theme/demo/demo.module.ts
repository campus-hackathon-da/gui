import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DemoRoutingModule } from './demo.routing';

@NgModule({
  imports: [
    CommonModule,
    DemoRoutingModule,
  ],
  declarations: [],
})
export class DemoModuleModule { }
