import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  templateUrl: './impressum.component.html',
  styleUrls: ['./impressum.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ImpressumComponent { }
