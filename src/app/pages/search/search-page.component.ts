import { Component, ViewEncapsulation, OnInit, ViewChild } from '@angular/core';
import { DataSource } from '@angular/cdk/collections';
import { MatPaginator } from '@angular/material';
import { RestClient } from '../../services/client/client.service';

import * as model from '../../services/model';
import * as rx from 'rxjs';

@Component({
  templateUrl: './search-page.component.html',
  styleUrls: ['./search-page.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class SearchPageComponent extends DataSource<any> implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;

  displayedColumns = ['name', 'type', 'trend'];

  dataChange: rx.BehaviorSubject<model.SearchResult[]> = new rx.BehaviorSubject<model.SearchResult[]>([]);
  searchModel: string = '';
  dataSource: DataSource<any>;
  showLoading: boolean = false;

  constructor(private client: RestClient) {
    super();
  }

  ngOnInit() {
    this.dataSource = this;
  }

  handleSearchRequest(query?: string) {
    this.showLoading = true;
    this.client.getSearchResults(query).subscribe(data => {
      this.showLoading = false;
      this.dataChange.next(data);
    });
  }

  getIconForTrend(trend: number) {

    if (trend < 0) {
      return 'assets/images/trend_bin_unknown.png';
    } else if (trend >= 0 && trend <= 0.2) {
      return 'assets/images/trend_bin_1.png';
    } else if (trend > 0.2 && trend <= 0.4) {
      return 'assets/images/trend_bin_2.png';
    } else if (trend > 0.4 && trend <= 0.6) {
      return 'assets/images/trend_bin_3.png';
    } else if (trend > 0.6 && trend <= 0.8) {
      return 'assets/images/trend_bin_4.png';
    } else if (trend > 0.8) {
      return 'assets/images/trend_bin_5.png';
    }
  }

  openUrl(url: string) {
    window.open(url, '_blank');
  }

  connect(): rx.Observable<model.SearchResult[]> {
    const displayDataChanges = [
      this.dataChange,
      this.paginator.page,
    ];

    return rx.Observable.merge(...displayDataChanges).map(() => {
      let data = this.data.slice();

      // Grab the page's slice of data.
      const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
      return data.splice(startIndex, this.paginator.pageSize);
    });
  }

  disconnect() { }

  get data(): model.SearchResult[] {

    return this.dataChange.value.sort((a, b) => {
      return b.trend - a.trend;
    });
  }


}
