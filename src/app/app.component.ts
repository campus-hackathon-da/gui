import { Component, ViewEncapsulation } from '@angular/core';


@Component({
  selector: 'nga-app',
  templateUrl: './app.compnent.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AppComponent { }
