# Darmstadt Hackathon GUI 

### Installation
```
# require:
* nodejs >= 6
* npm >= 3

# install dependencies
npm install
```

### Start local
```
npm start
```

### Build production
```
npm run build:aot
# or 
npm run build:prod
```
